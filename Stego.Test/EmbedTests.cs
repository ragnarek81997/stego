using SixLabors.ImageSharp;
using Stego.Core.Algorithms;
using Xunit;

namespace Stego.Test
{
    public class EmbedTests
    {
        [Fact]
        public void Embed_Decode()
        {
            var image = Image.Load(FileHelper.GetPathToImage());
            var fileBytes = System.IO.File.ReadAllBytes(FileHelper.GetPathToSecretData());
            byte[] resultBytes = null;
            using(var stego = new Core.Stego(FileHelper.GetPathToImage()))
            {
                stego.SetSecretData(fileBytes);
                var imageWithSecret = stego.Embed(AlgorithmEnum.Lsb);
                stego.SetImage(imageWithSecret);
                resultBytes = stego.Decode(AlgorithmEnum.Lsb);
            }            
            Assert.Equal(fileBytes, resultBytes);
        }

    }
}