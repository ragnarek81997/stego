using System;
using Xunit;

namespace Stego.Test
{
    public class EntryTests
    {
        [Fact]
        public void ConstructorFileSystem_FileNotFound()
        {
            System.IO.FileNotFoundException ex = Assert.Throws<System.IO.FileNotFoundException>(
                () =>
                {
                    var stego = new Core.Stego(Guid.NewGuid().ToString());
                });
        }
    }
}