using System;
using System.Collections;
using System.Collections.Generic;

namespace Stego.Core.Extensions
{
    public static class HammingCodeHelper
    {
        private static int MaxBlockLength = 16;
        
        public static int GetEncodedBitsCount(int decodedBitsLength)
        {
            return 
                decodedBitsLength + 
                decodedBitsLength / MaxBlockLength * GetControlBitsCountByDecoded(MaxBlockLength) + 
                (decodedBitsLength % MaxBlockLength > 0 ? GetControlBitsCountByDecoded(decodedBitsLength % MaxBlockLength) : 0);
        }
        
        private static int GetDecodedBitsCount(int encodedBitsLength)
        {
            var encodedBlockBitsCount = MaxBlockLength + GetControlBitsCountByDecoded(MaxBlockLength);
            var fullBlocksCount = encodedBitsLength / encodedBlockBitsCount;
            
            var notFullEncodedBlockBitsLength = encodedBitsLength - fullBlocksCount * encodedBlockBitsCount;
            var notFullBlockBitsCount = notFullEncodedBlockBitsLength > 0
                ? notFullEncodedBlockBitsLength - GetControlBitsCountByEncoded(notFullEncodedBlockBitsLength)
                : 0;
            
            return fullBlocksCount * MaxBlockLength + notFullBlockBitsCount;
        }
        
        private static int GetControlBitsCountByDecoded(int decodedBitsLength)
        {
            if (decodedBitsLength <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            return (int)Math.Ceiling(Math.Log(decodedBitsLength + 1 + Math.Log(decodedBitsLength + 1, 2), 2));
        }
        
        private static int GetControlBitsCountByEncoded(int encodedBitsLength)
        {
            if (encodedBitsLength <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            if ((int) Math.Log(encodedBitsLength, 2) == (int) Math.Ceiling(Math.Log(encodedBitsLength, 2)))
            {
                throw new ArgumentOutOfRangeException();
            }
            
            return (int)(Math.Log(encodedBitsLength, 2) + 1);
        }

        public static BitArray Encode(BitArray bits)
        {
            bits = new BitArray(bits);
            var encodedBits = BuildEncodedBitArray(bits);
            return CalculateControlBits(encodedBits, fixErrors: false);
        }
        
        public static BitArray Decode(BitArray encodedBits)
        {
            encodedBits = new BitArray(encodedBits);
            encodedBits = CalculateControlBits(encodedBits, fixErrors: true);
            return BuildDecodedBitArray(encodedBits);
        }

        private static BitArray BuildEncodedBitArray(BitArray bits)
        {
            var encodedBits = new BitArray(GetEncodedBitsCount(bits.Length));
            
            for (int bitPos = 0, encodedBitPos = 0; bitPos < bits.Length; bitPos += MaxBlockLength)
            {
                var blockLength = Math.Min(bits.Length - bitPos, MaxBlockLength);
                
                if (blockLength <= 0)
                {
                    continue;
                }
                
                var totalBitsCount = GetEncodedBitsCount(blockLength);
                
                for (int encodedBlockBit = 0, blockBitPos = 0; encodedBlockBit < totalBitsCount; encodedBlockBit += 1)
                {
                    var encodedBlockBitLog2 = Math.Log(encodedBlockBit + 1, 2);
                    if ((int) encodedBlockBitLog2 == (int) Math.Ceiling(encodedBlockBitLog2))
                    {
                        encodedBits.Set(encodedBitPos + encodedBlockBit, false);
                    }
                    else
                    {
                        encodedBits.Set(encodedBitPos + encodedBlockBit, bits[bitPos + blockBitPos]);
                        blockBitPos += 1;
                    }
                }

                encodedBitPos += totalBitsCount;
            }

            return encodedBits;
        }

        private static BitArray BuildDecodedBitArray(BitArray encodedBits)
        {
            var maxEncodedBlockLength = GetEncodedBitsCount(MaxBlockLength);
            
            var decodedBitsCount = GetDecodedBitsCount(encodedBits.Length);
            var decodedBits = new BitArray(decodedBitsCount);
            
            for (int encodedBitPos = 0, bitPos = 0; encodedBitPos < encodedBits.Length; encodedBitPos += maxEncodedBlockLength)
            {
                var encodedBlockLength = Math.Min(encodedBits.Length - encodedBitPos, maxEncodedBlockLength);
                
                if (encodedBlockLength <= 0)
                {
                    continue;
                }

                for (var encodedBlockBit = 0; encodedBlockBit < encodedBlockLength; encodedBlockBit += 1)
                {
                    var encodedBlockBitLog2 = Math.Log(encodedBlockBit + 1, 2);
                    if ((int) encodedBlockBitLog2 == (int) Math.Ceiling(encodedBlockBitLog2))
                    {
                        continue;
                    }
                    
                    decodedBits.Set(bitPos, encodedBits[encodedBitPos + encodedBlockBit]);
                    bitPos += 1;
                }
            }

            return decodedBits;
        }
        
        private static BitArray CalculateControlBits(BitArray encodedBits, bool fixErrors = false)
        {
            var maxEncodedBlockLength = GetEncodedBitsCount(MaxBlockLength);
                
            for (var encodedBitPos = 0; encodedBitPos < encodedBits.Length;)
            {
                var encodedBlockLength = Math.Min(encodedBits.Length - encodedBitPos, maxEncodedBlockLength);
                
                if (encodedBlockLength <= 0)
                {
                    continue;
                }
                
                var controlBitsDictionary = new Dictionary<int, bool>();
                var recalculatedControlBitsDictionary = new Dictionary<int, bool>();
                
                for (var encodedBlockBit = 0; encodedBlockBit < encodedBlockLength; encodedBlockBit  = (encodedBlockBit + 1) * 2 - 1)
                {
                    controlBitsDictionary[encodedBlockBit] = encodedBits[encodedBitPos + encodedBlockBit];
                    encodedBits.Set(encodedBitPos + encodedBlockBit, false);
                }
                
                for (var encodedBlockBit = 0; encodedBlockBit < encodedBlockLength; encodedBlockBit = (encodedBlockBit + 1) * 2 - 1)
                {
                    var controlBitValue = false;
                        
                    for (var i = encodedBlockBit; i < encodedBlockLength; i += (encodedBlockBit + 1) * 2)
                    {
                        for (var j = i; j < i + encodedBlockBit + 1 && j < encodedBlockLength; j++)
                        {
                            if (encodedBits[encodedBitPos + j])
                            {
                                controlBitValue = !controlBitValue;
                            }
                        }
                    }

                    recalculatedControlBitsDictionary[encodedBlockBit] = controlBitValue;
                    encodedBits.Set(encodedBitPos + encodedBlockBit, controlBitValue);
                }

                if (fixErrors)
                {
                    var errorSyndrome = 0;
                    for (var encodedBlockBit = 0; encodedBlockBit < encodedBlockLength; encodedBlockBit = (encodedBlockBit + 1) * 2 - 1)
                    {
                        if (controlBitsDictionary[encodedBlockBit] != recalculatedControlBitsDictionary[encodedBlockBit])
                        {
                            errorSyndrome += encodedBlockBit + 1;
                        }
                    }

                    if (errorSyndrome > 0)
                    {
                        var errorBitPosition = errorSyndrome - 1;
                        if (errorBitPosition >= 0 && errorBitPosition < encodedBlockLength)
                        {
                            encodedBits[encodedBitPos + errorBitPosition] = !encodedBits[encodedBitPos + errorBitPosition];
                        }
                    }
                }

                encodedBitPos += encodedBlockLength;
            }

            return encodedBits;
        }
    }
}