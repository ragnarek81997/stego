using System;

namespace Stego.Core.Exceptions
{
    public class DecodeException : Exception
    {
        private DecodeException()
        {
        }

        public DecodeException(string message)
            : base(message)
        {
        }

        public DecodeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}