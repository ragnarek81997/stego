using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Stego.Core.Extensions;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using Stego.Core.Core;
using Stego.Core.Exceptions;
using Stego.Core.Model;

namespace Stego.Core.Algorithms
{
    public class Lsb : StegoAlgorithm
    {
        private bool useHammingCode;
        
        public override Image<Rgba32> Embed(Image<Rgba32> baseImage, SecretData secret, Settings settings = null)
        {
            useHammingCode = settings?.UseHammingCode ?? false;
            
            var secretBits = secret.SecretWithLengthBits;

            if (useHammingCode)
            {
                secretBits = HammingCodeHelper.Encode(secretBits);
            }
            
            if (IsEmbedPossible(baseImage, secretBits.Length) == false)
            {
                throw new InvalidDataException("Secret data is to big for embending.");
            }
            
            var index = 0;
            var random = GetRandomGenenator(settings);
            var occupied = new HashSet<Tuple<int, int>>();
            
            while (index < secretBits.Length)
            {
                var width = random.Next(baseImage.Width);
                var height = random.Next(baseImage.Height);
                
                var pair = new Tuple<int, int>(width, height);
                if (occupied.Contains(pair))
                {
                    continue;
                }
                occupied.Add(pair);
                
                var pixel = baseImage[width, height];

                pixel.R = SetLsb(pixel.R, secretBits[index]);
                index += 1;
                
                if (index < secretBits.Length)
                {
                    pixel.B = SetLsb(pixel.B, secretBits[index]);
                    index += 1;
                }
                
                baseImage[width, height] = pixel;
            }

            return baseImage;
        }

        public override byte[] Decode(Image<Rgba32> stegoImage, Settings settings = null)
        {
            useHammingCode = settings?.UseHammingCode ?? false;
            
            var length = ReadSecretLength(stegoImage, settings) * 8;

            if (useHammingCode)
            {
                length = HammingCodeHelper.GetEncodedBitsCount(length);
            }
            
            if (length <= 0 || !IsEmbedPossible(stegoImage, length))
            {
                throw new DecodeException($"Cannot read secret from this image file. Readed secret length: {length}");
            }
            
            var bits = ReadBits(stegoImage, this.GetSecretDataLength(settings), length + this.GetSecretDataLength(settings), settings?.Key);

            if (useHammingCode)
            {
                bits = HammingCodeHelper.Decode(bits);
            }
            
            return bits.ToByteArray();
        }

        public override int ReadSecretLength(Image<Rgba32> stegoImage, Settings settings = null)
        {
            useHammingCode = settings?.UseHammingCode ?? false;
            
            var lengthBits = ReadBits(stegoImage, 0, this.GetSecretDataLength(settings), settings?.Key);
            
            if (useHammingCode)
            {
                lengthBits = HammingCodeHelper.Decode(lengthBits);
            }
            
            var bytes = lengthBits.ToByteArray();
            var length = BitConverter.ToInt32(bytes, 0);

            return length;
        }

        private BitArray ReadBits(Image<Rgba32> stegoImage, int start, int end, string key)
        {
            var length = end - start;
            if (length <= 0)
            {
                throw new InvalidDataException("end has to be > than start");
            }
            
            var bits = new BitArray(length);
            var index = 0;
            var random = GetRandomGenenator(key);
            var occupied = new HashSet<Tuple<int, int>>();
            
            while (index < end)
            {
                var width = random.Next(stegoImage.Width);
                var height = random.Next(stegoImage.Height);
                
                var pair = new Tuple<int, int>(width, height);
                if (occupied.Contains(pair))
                {
                    continue;
                }
                occupied.Add(pair);
                
                if (index < start)
                {
                    index += 2;
                    continue;
                }
                
                var pixel = stegoImage[width, height];
                var (bitR, bitB) = GetBitsFromPixel(pixel);
                
                bits.Set(index - start, bitR);
                index += 1;
                
                if (index < end)
                {
                    bits.Set(index - start, bitB);
                    index += 1;
                }
            }
            return bits;

        }

        private (bool R, bool B) GetBitsFromPixel(Rgba32 pixel)
        {
            var r = pixel.R;
            var b = pixel.B;
            
            var bitR = GetLsb(r);
            var bitB = GetLsb(b);
            
            return (bitR, bitB);
        }

        private static bool GetLsb(byte b)
        {
            return (b & 1) != 0;
        }

        private static byte SetLsb(byte b, bool value)
        {
            return value 
            // Make LSB 1
                ? (byte)(b | 1)
            // Make LSB 0
                : (byte)(b & 254);
        }

        public override bool IsEmbedPossible(Image<Rgba32> image, int secretLength)
        {
            return image.Width * image.Height * 2 >= secretLength;
        }
    }
}