using System;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace Stego.Core.Core
{
    public interface IStegoEntry : IDisposable
    {
         byte[] SecretDataBytes { get; }

         Image<Rgba32> StegoImage { get; }
    }
}