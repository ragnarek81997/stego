﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using Stego.Core.Algorithms;
using Stego.Core.Extensions;
using Stego.Core.Model;

namespace Stego.SimpleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var workingDirectory = Environment.CurrentDirectory;
            var projectDirectory = Directory.GetParent(workingDirectory).Parent?.Parent?.FullName;

            var settings = new Settings()
            {
                D = 20,
                Key = "cadabra_123456", 
                UseHammingCode = true
            };
            
            var algorithm = AlgorithmEnum.ZhaoKoch;
            
            var secretDataBytesLength = 200;

            var originalSecretDataBytes = GenerateRandomBytes(secretDataBytesLength, 564754);
            var decodedSecretDataBytes = new byte[0];
            
            using (var stego = new Stego.Core.Stego(Path.Combine(projectDirectory, "cat.png")))
            {
                stego.SetSettings(settings);
                stego.SetSecretData(originalSecretDataBytes);
                
                var stegoImage = stego.Embed(algorithm);
                stegoImage.Save(Path.Combine(projectDirectory, "stego_cat.png"));
            }
            
            using (var stego = new Stego.Core.Stego(Path.Combine(projectDirectory, "stego_cat.png")))
            {
                stego.SetSettings(settings);

                var verySecretBytes = stego.Decode(algorithm);
                decodedSecretDataBytes = verySecretBytes;
            }
            
            var originalSecretDataBits = new BitArray(originalSecretDataBytes);
            var decodedSecretDataBits = new BitArray(decodedSecretDataBytes);

            if (originalSecretDataBits.Length != decodedSecretDataBits.Length)
            {
                Console.WriteLine("Invalid data length");
            }
            else
            {
                var collection = new List<int>();
                for (var i = 0; i < originalSecretDataBits.Length; i++)
                {
                    if (originalSecretDataBits[i] != decodedSecretDataBits[i])
                    {
                        collection.Add(i);
                    }
                }
                Console.WriteLine(collection.Count);
                Console.WriteLine(string.Join(", ", collection));
            }
        }

        private static byte[] GenerateRandomBytes(int length, int seed)
        {
            var random = new Random(seed);
            var bytes = new byte[length];
            for (var i = 0; i < length; i++)
            {
                bytes[i] = (byte)random.Next(0, 255);
            }
            return bytes;
        }
    }
}